|     | NAME                                     |   REFERENCE |   COUNT |   CLUSTER |
|----:|:-----------------------------------------|------------:|--------:|----------:|
|   0 | Glucosa, sangre                          |      100092 |   27724 |         0 |
|   1 | Potasio, sangre                          |      100160 |   27541 |         0 |
|   2 | Sodio, sangre                            |      100190 |   27244 |         0 |
|   3 | Hematocrito sangre                       |      100100 |   26379 |         1 |
|   4 | Leucocitos recuento                      |      100119 |   19588 |         2 |
|   5 | Linfocitos absoluto analizado            |      100122 |   19574 |         2 |
|   6 | Linfocitos % analizado                   |      100120 |   19573 |         2 |
|   7 | VCM                                      |      100207 |   19514 |         2 |
|   8 | Plaquetas recuento                       |      100159 |   19514 |         2 |
|   9 | RDW Reed Distribut Width                 |      100180 |   19514 |         2 |
|  10 | Hematias recuento                        |      100098 |   19514 |         2 |
|  11 | CCMH concentraciion corpuscal media      |      100041 |   19513 |         2 |
|  12 | HCM hb corpusclar media                  |      100095 |   19513 |         2 |
|  13 | VPM Volumen Plaquetario Medio            |      100214 |   19513 |         2 |
|  14 | Hemoglobina concentracion                |      100102 |   19513 |         2 |
|  15 | Eosinofilos % analizado                  |      100074 |   19499 |         2 |
|  16 | Neutrofilos % analizado                  |      100140 |   19499 |         2 |
|  17 | Basofilos % analizado                    |      100020 |   19499 |         2 |
|  18 | Eosinofilos absolutos analizado          |      100076 |   19499 |         2 |
|  19 | Basofilos absolutos analizado            |      100022 |   19499 |         2 |
|  20 | Neutrofilos absoluto analizado           |      100143 |   19499 |         2 |
|  21 | Monocitos % analizado                    |      100136 |   19499 |         2 |
|  22 | Monocitos absoluto analizado             |      100138 |   19499 |         2 |
|  23 | Creatinina sangre                        |      100064 |   19404 |         2 |
|  24 | LUC % analizado                          |      100126 |   19289 |         2 |
|  25 | LUC absoluto analizado                   |      100127 |   19289 |         2 |
|  26 | Filtrado glomerular calculado            |      100086 |   12766 |         3 |
|  27 | Tiempo de protombina segundos            |      100195 |   11440 |         4 |
|  28 | Tiempo de protombina %                   |      100196 |   11425 |         4 |
|  29 | PCR                                      |      100156 |   11194 |         4 |
|  30 | Tiempo de tromboplastina parcial         |      100197 |   10205 |         5 |
|  31 | Bilirrubina adulto total                 |      100034 |    9889 |         5 |
|  32 | Cloruro, sangre                          |      100055 |    9879 |         5 |
|  33 | ASA aspartat aminotransferasa            |      100019 |    9846 |         5 |
|  34 | Gamma glutamil transpeptidasa            |      100090 |    9819 |         5 |
|  35 | Fosfatasa alcalina                       |      100087 |    9779 |         5 |
|  36 | ALAT Alanin aminotransferasa             |      100005 |    9742 |         5 |
|  37 | Lactato, sangre                          |      100116 |    9278 |         5 |
|  38 | Calcio                                   |      100037 |    9271 |         5 |
|  39 | Magnesio                                 |      100131 |    8924 |         5 |
|  40 | Calcio ionizado sangre arterial          |      100039 |    8920 |         5 |
|  41 | pCO2 sangre arterial                     |      100153 |    8827 |         5 |
|  42 | pO2 sangre arterial                      |      100155 |    8791 |         5 |
|  43 | Exceso de base sangre arterial           |      100079 |    8782 |         5 |
|  44 | Bicarbonato actual, sangre arterial      |      100026 |    8782 |         5 |
|  45 | Hemoglobina sangre                       |      100101 |    7615 |         6 |
|  46 | Carboxihemoglobina                       |      100040 |    7555 |         6 |
|  47 | Metahemoglobina, sangre                  |      100132 |    7554 |         6 |
|  48 | Proteinas totales                        |      100171 |    7492 |         6 |
|  49 | Bilirrubina directa                      |      100029 |    6787 |         7 |
|  50 | Saturacion de oxigeno %  sangre arterial |      100188 |    6447 |         7 |
|  51 | Lactato LDH                              |      100117 |    6433 |         7 |
|  52 | CO2 total sangre arterial                |      100057 |    5518 |         8 |
|  53 | PDW Platelet Distribut Width             |      100158 |    4982 |         8 |
|  54 | Macrocitos                               |      100128 |    4982 |         8 |
|  55 | Microcitos                               |      100129 |    4982 |         8 |
|  56 | HDW hb distributiva media                |      100096 |    4982 |         8 |
|  57 | Hematias hipocromas %                    |      100099 |    4982 |         8 |
|  58 | CHCM concentracion hemoglobina           |      100052 |    4982 |         8 |
|  59 | Dimero D cuantitativo serum              |      100072 |    3714 |         9 |
|  60 | Albumina                                 |      100006 |    3243 |         9 |
|  61 | Bilirrubina indirecta                    |      100032 |    3115 |         9 |
|  62 | Troponina alta sensibilidad              |      100205 |    3024 |         9 |
|  63 | BUN                                      |      100035 |    2766 |         9 |
|  64 | Ferritina                                |      100084 |    2523 |         9 |
|  65 | Calcio ionizado sangre venosa            |      100038 |    2512 |         9 |
|  66 | pCO2 sangre venosa                       |      100152 |    2497 |         9 |
|  67 | Exceso de base sangre venosa             |      100078 |    2496 |         9 |
|  68 | Bicarbonato actual, sangre venosa        |      100025 |    2496 |         9 |
|  69 | pO2 sangre venosa                        |      100154 |    2483 |         9 |
|  70 | Trigliceridos                            |      100203 |    2449 |         9 |
|  71 | Saturacion de oxigeno %  sangre venosa   |      100187 |    2393 |         9 |
|  72 | Fosforo, plasma                          |      100088 |    2384 |         9 |
|  73 | Procalcitonina plasma                    |      100165 |    2066 |         9 |
|  74 | Fibrinogeno                              |      100085 |    2013 |         9 |
|  75 | Monocitos % manual                       |      100137 |    1998 |         9 |
|  76 | Linfocitos absoluto manual               |      100123 |    1997 |         9 |
|  77 | Monocitos absoluto manual                |      100139 |    1997 |         9 |
|  78 | Eosinofilos % manual                     |      100075 |    1997 |         9 |
|  79 | Neutrofilos % manual no segmentado       |      100141 |    1997 |         9 |
|  80 | Basofilos % manual                       |      100021 |    1997 |         9 |
|  81 | Neutrofilos % manual segmentado          |      100142 |    1997 |         9 |
|  82 | Eosinofilos absolutos manual             |      100077 |    1997 |         9 |
|  83 | Basofilos absolutos manual               |      100023 |    1997 |         9 |
|  84 | Linfocitos % manual                      |      100121 |    1997 |         9 |
|  85 | Neutrofilos absoluto manual              |      100144 |    1953 |         9 |
|  86 | Densidad orina                           |      100071 |    1938 |         9 |
|  87 | Colesterol total                         |      100061 |    1919 |         9 |
|  88 | TCO2 venosa                              |      100193 |    1851 |         9 |
|  89 | Bicarbonato estandard, sangre venosa     |      100028 |    1850 |         9 |
|  90 | Amilasa                                  |      100008 |    1790 |         9 |
|  91 | Lipasa                                   |      100124 |    1693 |         9 |
|  92 | Creatinina quinasa                       |      100068 |    1401 |         9 |
|  93 | Acido urico plasma                       |      100003 |    1234 |         9 |
|  94 | TCO2 arterial                            |      100192 |    1099 |         9 |
|  95 | Bicarbonato estandard, sangre arterial   |      100027 |    1099 |         9 |
|  96 | Oxihemoglobina fraccion sangre           |      100151 |    1065 |         9 |
|  97 | Hierro                                   |      100107 |     880 |         9 |
|  98 | Zinc                                     |      100216 |     686 |         9 |
|  99 | Transferrina                             |      100201 |     644 |         9 |
| 100 | Saturación transferrina, serum           |      100202 |     640 |         9 |
| 101 | Prealbumina serum                        |      100162 |     557 |         9 |
| 102 | Tirotropina serum                        |      100199 |     546 |         9 |
| 103 | Coeficiente proteinas/Creatinina         |      100070 |     430 |         9 |
| 104 | Tacròlimus FK506                         |      100191 |     428 |         9 |
| 105 | CKMB creatin quinasa MB                  |      100053 |     416 |         9 |
| 106 | VSG velocidad sedimentacion golbular     |      100215 |     394 |         9 |
| 107 | Osmolalidad                              |      100149 |     385 |         9 |
| 108 | Tiroxina libre serum                     |      100200 |     376 |         9 |
| 109 | Vitamina B12                             |      100209 |     374 |         9 |
| 110 | Colesterol HDL                           |      100062 |     371 |         9 |
| 111 | Colesterol LDL                           |      100059 |     362 |         9 |
| 112 | NT-proBNP plasma                         |      100145 |     358 |         9 |
| 113 | Acido folico serum                       |      100002 |     321 |         9 |
| 114 | Reticulocitos % alta fluor               |      100182 |     301 |         9 |
| 115 | Reticulocitos % manual                   |      100183 |     301 |         9 |
| 116 | Reticulocitos % baja fluor               |      100184 |     301 |         9 |
| 117 | Reticulocitos % media fluor              |      100185 |     301 |         9 |
| 118 | Reticulocitos absoluta manual            |      100186 |     301 |         9 |
| 119 | Receptor soluble transfe                 |      100181 |     289 |         9 |
| 120 | Osmolalidad urinaria                     |      100150 |     271 |         9 |
| 121 | Hemoglobina concentracion retic          |      100103 |     259 |         9 |
| 122 | Proteinas orina reciente                 |      100170 |     254 |         9 |
| 123 | Anion Gap sangre venosa                  |      100009 |     250 |         9 |
| 124 | Haptoglobina serum                       |      100094 |     247 |         9 |
| 125 | Creatinina orina                         |      100067 |     237 |         9 |
| 126 | Volumen minuto                           |      100213 |     234 |         9 |
| 127 | Creatinina orina 24h                     |      100065 |     208 |         9 |
| 128 | Creatinina aclaramiento                  |      100066 |     191 |         9 |
| 129 | Apolipoproteina B                        |      100018 |     191 |         9 |
| 130 | CD3 linfocitos  % sangre total           |      100042 |     186 |         9 |
| 131 | CD3 valor absoluto sangre total          |      100043 |     186 |         9 |
| 132 | CD4 linfocitos % sangre total            |      100044 |     186 |         9 |
| 133 | CD8 linfocitos % sangre total            |      100047 |     186 |         9 |
| 134 | CD4 valor absoluto sangre total          |      100045 |     185 |         9 |
| 135 | CD8 valor absoluto sangre total          |      100046 |     185 |         9 |
| 136 | Proteinas orina 24h                      |      100169 |     185 |         9 |
| 137 | Proteinas beta glob % serum              |      100174 |     171 |         9 |
| 138 | Proteinas gamma glob % serum             |      100175 |     171 |         9 |
| 139 | Albumina proteina % serum                |      100007 |     171 |         9 |
| 140 | Proteinas Alfa 1 glob % serum            |      100172 |     171 |         9 |
| 141 | Proteinas Alfa 2 glob % serum            |      100173 |     171 |         9 |
| 142 | Cociente  microalbumina/creatinina       |      100058 |     123 |         9 |
| 143 | Potasio orina 24h                        |      100161 |     112 |         9 |
| 144 | PTH parathormona serum                   |      100179 |     112 |         9 |
| 145 | Vitamina D3 25 hidroxi serum             |      100212 |     108 |         9 |
| 146 | Anion Gap sangre arterial                |      100010 |      97 |         9 |
| 147 | CEA antigeno carcinoembrionario          |      100048 |      94 |         9 |
| 148 | Antigeno CA-19.9                         |      100015 |      79 |         9 |
| 149 | Microalbuminuria                         |      100134 |      76 |         9 |
| 150 | Factor C3 complemento                    |      100081 |      75 |         9 |
| 151 | Factor C4 complemento                    |      100082 |      75 |         9 |
| 152 | Homocisteina basal                       |      100108 |      70 |         9 |
| 153 | IgG dosificacion serum                   |      100111 |      70 |         9 |
| 154 | Immunoglobulina IgM serum                |      100113 |      64 |         9 |
| 155 | Antigeno CA-125                          |      100013 |      62 |         9 |
| 156 | Immunoglobulina IgA serum                |      100112 |      59 |         9 |
| 157 | Vitamina D3 1,25 hidroxi serum           |      100211 |      57 |         9 |
| 158 | AFP Alfafetoproteina                     |      100004 |      50 |         9 |
| 159 | Antigeno CA-153                          |      100014 |      49 |         9 |
| 160 | Enolasa neuronal especifica              |      100073 |      47 |         9 |
| 161 | Microalbumina orina reciente             |      100133 |      47 |         9 |
| 162 | Pro-gastrin releasing peptide            |      100166 |      38 |         9 |
| 163 | CH50                                     |      100051 |      38 |         9 |
| 164 | SCC                                      |      100189 |      38 |         9 |
| 165 | Antigeno prostatico especifico           |      100017 |      36 |         9 |
| 166 | Microglobulina beta 2                    |      100135 |      35 |         9 |
| 167 | Anticuerpos antiperoxidasa               |      100012 |      29 |         9 |
| 168 | Cortisol serum                           |      100063 |      27 |         9 |
| 169 | Triodetironina serum                     |      100204 |      26 |         9 |
| 170 | Hemoglobina glicada sangre total         |      100104 |      26 |         9 |
| 171 | Proteina S libre plasma                  |      100176 |      22 |         9 |
| 172 | bhCG Gonadotropina corionica             |      100024 |      21 |         9 |
| 173 | Procalcitonina serum                     |      100164 |      21 |         9 |
| 174 | KAPPA cadenas ligeras libre              |      100115 |      21 |         9 |
| 175 | Proteina C funcional plasma              |      100168 |      21 |         9 |
| 176 | Cadenas ligeras Lambda libre             |      100036 |      21 |         9 |
| 177 | Magnesio orina reciente                  |      100130 |      18 |         9 |
| 178 | IgG antibeta 2 glicoproteina             |      100110 |      16 |         9 |
| 179 | Ac IgM antibeta 2 glicoproteico          |      100001 |      15 |         9 |
| 180 | IGE total serum                          |      100109 |      13 |         9 |
| 181 | Anticuerpos antiDNA bicatenario          |      100011 |      12 |         9 |
| 182 | Celulas NK                               |      100049 |      12 |         9 |
| 183 | Celulas NK totales                       |      100050 |      12 |         9 |
| 184 | Coeficiente calcio/creatinina            |      100069 |      12 |         9 |
| 185 | PRL prolactina serum                     |      100163 |      11 |         9 |
| 186 | Excrecion calcio                         |      100080 |       8 |         9 |
| 187 | Vitamina B1                              |      100208 |       7 |         9 |
| 188 | PSA libre                                |      100178 |       7 |         9 |
| 189 | Fosforo orina 24h                        |      100089 |       6 |         9 |
| 190 | Globulina ligado hormonas                |      100091 |       5 |         9 |
| 191 | Proteina S100                            |      100177 |       5 |         9 |
| 192 | Vitamina B6                              |      100210 |       5 |         9 |
| 193 | Glucosa orina 24h                        |      100093 |       5 |         9 |
| 194 | Factor reumatoide serum                  |      100083 |       5 |         9 |
| 195 | PCR ultrasensible                        |      100157 |       4 |         9 |
| 196 | Testosterona directa serum               |      100194 |       4 |         9 |
| 197 | Hematias sedimiento                      |      100097 |       4 |         9 |
| 198 | Urea                                     |      100206 |       2 |         9 |
| 199 | LPA                                      |      100125 |       2 |         9 |
| 200 | Oncoproteina HER-2 NEU serum             |      100148 |       2 |         9 |
| 201 | Tiroglobulina serum                      |      100198 |       2 |         9 |
| 202 | Leucocitos sedimiento                    |      100118 |       2 |         9 |
| 203 | Insulina IRI serum                       |      100114 |       1 |         9 |