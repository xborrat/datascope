|     | NAME                                                                                    |   REFERENCE |   COUNT |   CLUSTER |
|----:|:----------------------------------------------------------------------------------------|------------:|--------:|----------:|
|   0 | Saturación de oxígeno por pulsioximetría                                                |     2000007 |  136718 |         0 |
|   1 | Presión arterial no invasiva sistólica                                                  |     2000034 |  114139 |         1 |
|   2 | Presión arterial no invasiva diastólica                                                 |     2000036 |  113970 |         1 |
|   3 | Temperatura axilar                                                                      |     2000052 |  107122 |         2 |
|   4 | Frecuencia cardíaca por electrocardiografía                                             |     2000025 |   81584 |         3 |
|   5 | Frecuencia cardíaca por oscilometría                                                    |     2000022 |   81474 |         3 |
|   6 | Frecuencia respiratoria por impedancia                                                  |     2000003 |   65767 |         4 |
|   7 | Presión arterial invasiva sistólica                                                     |     2000043 |   43077 |         5 |
|   8 | Presión arterial invasiva diastólica                                                    |     2000045 |   42797 |         5 |
|   9 | Presión arterial invasiva media                                                         |     2000044 |   42433 |         5 |
|  10 | Dispositivo para la administración de oxígeno en respiración espontánea                 |     2400029 |   37291 |         6 |
|  11 | Frecuencia respiratoria por inspección                                                  |     2000001 |   31102 |         7 |
|  12 | Ritmo cardíaco                                                                          |     2000073 |   30039 |         7 |
|  13 | Flujo de oxígeno en respiración espontána                                               |     2400033 |   22800 |         8 |
|  14 | Posición del paciente                                                                   |     2100002 |   22172 |         8 |
|  15 | Presión arterial no invasiva media                                                      |     2000035 |   20231 |         8 |
|  16 | Modalidad de acceso a la vía aérea para respiración                                     |     2400013 |   19488 |         8 |
|  17 | Posición de la cabecera de la cama respecto a la horizontal                             |     2100001 |   18986 |         8 |
|  18 | Origen de la muestra sanguinea                                                          |     2200000 |   16739 |         8 |
|  19 | Escala de coma de Glasgow                                                               |     2100021 |   15825 |         8 |
|  20 | Volumen  minuto espiratorio en VM invasiva                                              |     2400168 |   14488 |         8 |
|  21 | Presión positiva inspiratoria pico en VM invasiva                                       |     2400169 |   14294 |         8 |
|  22 | Fracción inspiratoria de oxígeno en respiración espontánea                              |     2000078 |   13356 |         8 |
|  23 | Volumen corriente espiratorio en VM invasiva                                            |     2400167 |   11962 |         8 |
|  24 | Frecuencia respiratoria actual en VM invasiva                                           |     2400164 |   11944 |         8 |
|  25 | Fracción inspiratoria de oxígeno en VM invasiva                                         |     2400137 |   11172 |         8 |
|  26 | Presión venosa central invasiva media                                                   |     2000050 |   10059 |         8 |
|  27 | PEEP actual medida en VM invasiva                                                       |     2400172 |    8329 |         8 |
|  28 | Diámetro de la pupila izquierda                                                         |     2100008 |    7876 |         8 |
|  29 | Reflejo fotomotor de la pupila derecha                                                  |     2100007 |    7851 |         8 |
|  30 | Diámetro de la pupila derecha                                                           |     2100006 |    7786 |         8 |
|  31 | Nivel de sedación según escala de RASS                                                  |     2100017 |    7754 |         8 |
|  32 | Tipo de tubo/cánula endotraqueal/endobronquial                                          |     2400016 |    7620 |         8 |
|  33 | PEEP pautada en VM invasiva                                                             |     2400162 |    7517 |         8 |
|  34 | Modelo de ventilador mecánico invasivo                                                  |     2400135 |    7418 |         8 |
|  35 | Presión positiva inspiratoria meseta en VM invasiva                                     |     2400170 |    7266 |         8 |
|  36 | Diámetro del tubo/cánula endotraqueal/endobronquial                                     |     2400017 |    7081 |         8 |
|  37 | Concentración alveolar mínima gas halogenado en VM anestesia                            |     2400121 |    7028 |         8 |
|  38 | Presión parcial inspiratoria de anhídrido carbónico al VM anestesia                     |     2400112 |    7022 |         8 |
|  39 | Fracción espiratoria de oxígeno del VM anestesia                                        |     2400111 |    7005 |         8 |
|  40 | Fracción inspiratoria de oxígeno al VM anestesia                                        |     2400110 |    6992 |         8 |
|  41 | Frecuencia respiratoria actual del paciente en VM anestesia                             |     2400128 |    6875 |         8 |
|  42 | Fracción inspiratoria de desflorano al VM anestesia                                     |     2400119 |    6771 |         8 |
|  43 | Fracción espiratoria de desflorano del VM anestesia                                     |     2400120 |    6771 |         8 |
|  44 | Presión parcial espiratoria de anhídrido carbónico del VM anestesia                     |     2400113 |    6758 |         8 |
|  45 | Volumen corriente pautado en VM anestesia                                               |     2400123 |    6738 |         8 |
|  46 | Presión pico en VM anestesia                                                            |     2400131 |    6713 |         8 |
|  47 | Modalidad ventilatoria en VM invasiva                                                   |     2400136 |    6711 |         8 |
|  48 | Potencial de hidrogeniones                                                              |     2200001 |    6690 |         8 |
|  49 | PEEP actual medida en VM anestesia                                                      |     2400133 |    6689 |         8 |
|  50 | Presión parcial de O2 por sensor transcutaneo                                           |     2000009 |    6688 |         8 |
|  51 | Presión meseta en VM anestesia                                                          |     2400132 |    6685 |         8 |
|  52 | Presión parcial de oxígeno                                                              |     2200002 |    6675 |         8 |
|  53 | Calcio iónico                                                                           |     2200023 |    6672 |         8 |
|  54 | Potasio                                                                                 |     2200022 |    6662 |         8 |
|  55 | Volumen corriente espiratorio en VM anestesia                                           |     2400129 |    6649 |         8 |
|  56 | Bicarbonato actual                                                                      |     2200004 |    6628 |         8 |
|  57 | Volumen minuto espiratorio en VM anestesia                                              |     2400130 |    6627 |         8 |
|  58 | Sistema de humidificación en administración de oxígeno en respiración espontánea        |     2400030 |    6593 |         8 |
|  59 | Exceso de base                                                                          |     2200005 |    6586 |         8 |
|  60 | Cloro                                                                                   |     2200024 |    6580 |         8 |
|  61 | Reflejo fotomotor de la pupila izquierda                                                |     2100009 |    6478 |         8 |
|  62 | Lactato                                                                                 |     2200029 |    6456 |         8 |
|  63 | Volumen corriente inspiratorio real en VM invasiva                                      |     2400166 |    6392 |         8 |
|  64 | Localización del dolor                                                                  |     2100031 |    6289 |         8 |
|  65 | Frecuencia respiratoria correspondiente a respiraciones espontáneas en VM invasiva      |     2400165 |    6161 |         8 |
|  66 | Sodio                                                                                   |     2200021 |    5902 |         8 |
|  67 | Hemoglobina                                                                             |     2200019 |    5786 |         8 |
|  68 | Escala de valoración del dolor en pacientes sedados e intubados                         |     2100028 |    5585 |         8 |
|  69 | Grado de delirio según escala CAM-ICU                                                   |     2100033 |    5244 |         8 |
|  70 | Volumen corriente inspiratorio pautado en VM invasiva                                   |     2400140 |    4640 |         8 |
|  71 | Peso                                                                                    |     2100164 |    4553 |         8 |
|  72 | Frecuencia cardíaca por pulso                                                           |     2000020 |    4220 |         8 |
|  73 | Orificio de inserción del tubo endotraqueal                                             |     2400018 |    3327 |         8 |
|  74 | Presión soporte/asistida por encima de PEEP en VM invasiva                              |     2400143 |    2668 |         8 |
|  75 | Temperatura arteria pulmonar                                                            |     2000067 |    2402 |         8 |
|  76 | Presión parcial de CO2 al final de la espiración por capnografía                        |     2000013 |    2375 |         8 |
|  77 | Temperatura esofágica                                                                   |     2000059 |    2372 |         8 |
|  78 | Tipo de dispositivo de marcapasos transitorio                                           |     2400266 |    1937 |         8 |
|  79 | Hematocrito                                                                             |     2200020 |    1919 |         8 |
|  80 | Modalidad de marcapasos                                                                 |     2400267 |    1888 |         8 |
|  81 | Frecuencia marcapasos programada                                                        |     2400268 |    1711 |         8 |
|  82 | Indicador de batería del marcapasos                                                     |     2400271 |    1702 |         8 |
|  83 | Frecuencia cardíaca por pletismografia                                                  |     2000023 |    1683 |         8 |
|  84 | Umbral de sensado ventricular                                                           |     2400269 |    1617 |         8 |
|  85 | Intensidad de estímulo ventricualr                                                      |     2400270 |    1610 |         8 |
|  86 | Talla                                                                                   |     2100165 |    1564 |         8 |
|  87 | Escala de Aldrete modificada                                                            |     2100151 |    1041 |         8 |
|  88 | Grado de delirio según escala Nursing Delirium Screaning Scale                          |     2100034 |     935 |         8 |
|  89 | Presión de entrada en TCSR                                                              |     2400344 |     849 |         8 |
|  90 | Presión de retorno en TCSR                                                              |     2400347 |     849 |         8 |
|  91 | Intensidad de pulso arteria pedia derecha                                               |     2100094 |     819 |         8 |
|  92 | Presión transmembrana en TCSR                                                           |     2400346 |     795 |         8 |
|  93 | Escala NIH para valoración del ictus: Puntuación global                                 |     2100037 |     783 |         8 |
|  94 | Compliancia dinámica torácica en VM invasiva                                            |     2400176 |     747 |         8 |
|  95 | Intensidad de pulso arteria pedia izquierda                                             |     2100095 |     648 |         8 |
|  96 | Escala NIH: Paresia facial                                                              |     2100043 |     644 |         8 |
|  97 | Escala NIH: Preguntas verbales                                                          |     2100039 |     640 |         8 |
|  98 | Escala NIH: Lenguaje                                                                    |     2100050 |     639 |         8 |
|  99 | Escala NIH: Disartria                                                                   |     2100051 |     638 |         8 |
| 100 | Escala NIH: Sensibilidad                                                                |     2100049 |     637 |         8 |
| 101 | Escala NIH: Paresia de extremidades superior I                                          |     2100045 |     635 |         8 |
| 102 | Escala NIH: Paresia de extremidades inferior I                                          |     2100047 |     635 |         8 |
| 103 | Escala NIH: Campos visuales                                                             |     2100042 |     634 |         8 |
| 104 | Escala NIH: O?rdenes motoras                                                            |     2100040 |     633 |         8 |
| 105 | Escala NIH: Paresia de extremidades superior D                                          |     2100044 |     633 |         8 |
| 106 | Escala NIH: Paresia de extremidades inferior D                                          |     2100046 |     633 |         8 |
| 107 | Escala NIH: Ataxia de las extremidades                                                  |     2100048 |     630 |         8 |
| 108 | Escala NIH: Nivel de conciencia                                                         |     2100038 |     629 |         8 |
| 109 | Escala NIH: Mirada conjugada                                                            |     2100041 |     628 |         8 |
| 110 | Escala NIH: Extincio?n-Negligencia                                                      |     2100052 |     613 |         8 |
| 111 | Flujo de sangre corazón izquierdo                                                       |     2400303 |     604 |         8 |
| 112 | Revoluciones bomba centrífuga corazón izquierdo                                         |     2400302 |     517 |         8 |
| 113 | Presión de efluente en TCSR                                                             |     2400348 |     517 |         8 |
| 114 | Valoración del grado de bloqueo motor post-anestesia regional                           |     2100150 |     494 |         8 |
| 115 | Respiraciones espontáneas en VM no invasiva                                             |     2400076 |     488 |         8 |
| 116 | Temperatura de humidificación activa                                                    |     2400031 |     467 |         8 |
| 117 | Extracción horaria pautada en TCSR                                                      |     2400330 |     461 |         8 |
| 118 | Volumen corriente espiratorio en VM no invasiva                                         |     2400077 |     448 |         8 |
| 119 | Grado de dolor en demencia avanzada                                                     |     2100027 |     441 |         8 |
| 120 | Volumen  minuto espiratorio en VM no invasiva                                           |     2400078 |     436 |         8 |
| 121 | Fracción inspiratoria de oxígeno en VM no invasiva                                      |     2400069 |     415 |         8 |
| 122 | Fugas de volumen inspiratorio en VM no invasiva                                         |     2400079 |     414 |         8 |
| 123 | Flujo de la solución dializante en TCSR                                                 |     2400324 |     395 |         8 |
| 124 | Flujo de sangre corazón derecho                                                         |     2400301 |     394 |         8 |
| 125 | Tipo de anticoagulación utilizada en TCSR                                               |     2400332 |     390 |         8 |
| 126 | Flujo de sangre en TCSR                                                                 |     2400322 |     388 |         8 |
| 127 | Modalidad de terapia contínua de sustitución renal                                      |     2400319 |     381 |         8 |
| 128 | Modalidad ventilatoria no invasiva                                                      |     2400068 |     373 |         8 |
| 129 | Solución dializante en TCSR                                                             |     2400323 |     372 |         8 |
| 130 | Tipo de hemofiltro en TCSR                                                              |     2400320 |     371 |         8 |
| 131 | Presión positiva espiratoria en VM no invasiva                                          |     2400071 |     369 |         8 |
| 132 | Tipo de dispositivo utillizado para TCSR                                                |     2400318 |     356 |         8 |
| 133 | Presión positiva inspiratoria en VM no invasiva                                         |     2400070 |     344 |         8 |
| 134 | Tipo de mascarilla para ventilación no invasiva                                         |     2400067 |     342 |         8 |
| 135 | Saturación de oxígeno                                                                   |     2200006 |     327 |         8 |
| 136 | Tipo de dispositivo de ventilación no invasiva                                          |     2400066 |     321 |         8 |
| 137 | Modalidad de asistencia circulatoria                                                    |     2400299 |     319 |         8 |
| 138 | Revoluciones bomba centrífuga corazón derecho                                           |     2400300 |     316 |         8 |
| 139 | Dispositivo utilizado para la asistencia circulatoria                                   |     2400298 |     278 |         8 |
| 140 | Dosis de calcio en TCSR                                                                 |     2400340 |     269 |         8 |
| 141 | Presión balón pneumotaponamiento tras control                                           |     2400022 |     256 |         8 |
| 142 | Presión parcial de CO2 al final de la espiración por VMI                                |     2000015 |     250 |         8 |
| 143 | Dosis de citrato en TCSR                                                                |     2400336 |     250 |         8 |
| 144 | Flujo de sangre                                                                         |     2400256 |     247 |         8 |
| 145 | Flujo de la solución de reposición en TCSR                                              |     2400328 |     234 |         8 |
| 146 | Revoluciones de la bomba centrífuga de sangre                                           |     2400255 |     230 |         8 |
| 147 | Solución de reposición en TCSR                                                          |     2400325 |     229 |         8 |
| 148 | Lugar de aporte de la solución de reposición en TCSR                                    |     2400329 |     228 |         8 |
| 149 | Flujo de oxígeno                                                                        |     2400258 |     210 |         8 |
| 150 | Temperatura venosa (premembrana al oxigenador)                                          |     2400264 |     202 |         8 |
| 151 | Temperatura calentador externo en TCSR                                                  |     2400343 |     193 |         8 |
| 152 | Temperatura arterial (postmembrana del oxigenador)                                      |     2400265 |     193 |         8 |
| 153 | Grado de dolor según escala visual analógica                                            |     2100026 |     177 |         8 |
| 154 | Temperatura del agua del sistema hipotermia terapéutica                                 |     2400396 |     173 |         8 |
| 155 | Escala de West-Haven                                                                    |     2100147 |     161 |         8 |
| 156 | Saturación de oxígeno venosa antes del oxigenador (premembrana)                         |     2400315 |     136 |         8 |
| 157 | Relación pupilar                                                                        |     2100015 |     136 |         8 |
| 158 | Solución de calcio en TCSR                                                              |     2400338 |     136 |         8 |
| 159 | Perímetro abominal                                                                      |     2100070 |     129 |         8 |
| 160 | Flujo de calcio en TCSR                                                                 |     2400339 |     128 |         8 |
| 161 | Temperatura objetivo a alcanzar en hipotermia terapéutica                               |     2400395 |     126 |         8 |
| 162 | Temperatura esofágica en hipotermia terapéutica                                         |     2400398 |     123 |         8 |
| 163 | FiO2 mezclador                                                                          |     2400309 |     113 |         8 |
| 164 | Presión venosa  premembrana del módulo oxigenador                                       |     2400312 |     111 |         8 |
| 165 | Presión arterial postmembrana del módulo oxigenador                                     |     2400314 |     110 |         8 |
| 166 | Flujo de heparina en TCSR                                                               |     2400333 |     109 |         8 |
| 167 | Solución de citrato en TCSR                                                             |     2400334 |     109 |         8 |
| 168 | Flujo de solución de citrato en TCSR                                                    |     2400335 |     104 |         8 |
| 169 | FiO2 del mezclador                                                                      |     2400257 |     103 |         8 |
| 170 | Modalidad de terapia de oxigenación extracorpórea                                       |     2400253 |     100 |         8 |
| 171 | Grado de encefalopatía hepática (West-Haven)                                            |     2100035 |      93 |         8 |
| 172 | Temperatura endovascular                                                                |     2000068 |      91 |         8 |
| 173 | Presión venosa central medida por columna de agua                                       |     2000049 |      87 |         8 |
| 174 | Número de identificación del hemofiltro para TCSR                                       |     2400321 |      87 |         8 |
| 175 | Dispositivo para la hipotermia terapéutica                                              |     2400393 |      87 |         8 |
| 176 | Ratio de asistencia de contrapulsación                                                  |     2400283 |      85 |         8 |
| 177 | Fase de enfriamiento / calentamiento                                                    |     2400394 |      84 |         8 |
| 178 | Concentración de óxido nítrico                                                          |     2400059 |      83 |         8 |
| 179 | Potencial de hidrogeniones en orina                                                     |     2200051 |      82 |         8 |
| 180 | Concentración de dioxido de nitrógeno                                                   |     2400060 |      79 |         8 |
| 181 | Fracción inspiratoria de oxígeno en dispositivo de óxido nítrico                        |     2400061 |      78 |         8 |
| 182 | Grado de dolor coronario según escala numérica ordinal                                  |     2100055 |      78 |         8 |
| 183 | Tipo de módulo oxigenador                                                               |     2400254 |      77 |         8 |
| 184 | Intensidad de estímulo auricular                                                        |     2400276 |      71 |         8 |
| 185 | Tipo de balón de contrapulsación                                                        |     2400281 |      71 |         8 |
| 186 | Sincronización de balón de contrapulsación                                              |     2400282 |      71 |         8 |
| 187 | Modelo de dispositivo para administración de óxido nítrico                              |     2400058 |      70 |         8 |
| 188 | Umbral de sensado auricular                                                             |     2400275 |      70 |         8 |
| 189 | Flujo de entrada de aire fresco al VM anestesia                                         |     2400106 |      61 |         8 |
| 190 | Dispositivo para realizar asistencia respiratoria extracorporea                         |     2400252 |      58 |         8 |
| 191 | Tiempo desde inicio respiración hasta alcanzar IPAP                                     |     2400075 |      57 |         8 |
| 192 | Cálculo de riesgo nutricional (Malnutrition Universal Screening Tool)                   |     2100149 |      53 |         8 |
| 193 | Intensidad de pulso arteria poplítea derecha                                            |     2100090 |      49 |         8 |
| 194 | Temperatura vesical en hipotermia terapéutica                                           |     2400397 |      49 |         8 |
| 195 | Modelo de dispositivo                                                                   |     2400304 |      48 |         8 |
| 196 | Grado de dolor según escala visual categórica                                           |     2100025 |      44 |         8 |
| 197 | Intensidad de pulso arteria poplítea izquierda                                          |     2100091 |      44 |         8 |
| 198 | Presión positiva contínua en VM no invasiva                                             |     2400072 |      40 |         8 |
| 199 | Nivel de sedación según escala de Ramsay                                                |     2100016 |      37 |         8 |
| 200 | Retraso aurículo-ventricular                                                            |     2400277 |      37 |         8 |
| 201 | KCl extra añadido a solución de reposición en TCSR                                      |     2400326 |      37 |         8 |
| 202 | Tiempo inspiratorio en VM invasiva                                                      |     2400154 |      35 |         8 |
| 203 | Intensidad de pulso arteria femoral derecha                                             |     2100088 |      33 |         8 |
| 204 | Intensidad de pulso arteria tibial derecha                                              |     2100092 |      30 |         8 |
| 205 | coloración extremidad inferior derecha                                                  |     2100113 |      26 |         8 |
| 206 | Presión positiva al final de espiración en VM no invasiva                               |     2400074 |      26 |         8 |
| 207 | KCl extra añadido a solución dializante en TCSR                                         |     2400341 |      25 |         8 |
| 208 | KH2PO4 extra añadido a solución de reposición en TCSR                                   |     2400327 |      24 |         8 |
| 209 | sensibilidad extremidad inferior derecha                                                |     2100109 |      23 |         8 |
| 210 | Flujo espiratorio máximo en respiración espontánea                                      |     2100065 |      21 |         8 |
| 211 | Intensidad de pulso arteria tibial izquierda                                            |     2100093 |      21 |         8 |
| 212 | Tipo de dispositivo de VM anestesia                                                     |     2400104 |      19 |         8 |
| 213 | Intensidad de pulso arteria radial derecha                                              |     2100086 |      19 |         8 |
| 214 | coloración extremidad inferior izquierda                                                |     2100114 |      19 |         8 |
| 215 | Modalidad ventilatoria en VM anestesia                                                  |     2400105 |      18 |         8 |
| 216 | Glucosuria en tira reactiva neonatología                                                |     2200062 |      17 |         8 |
| 217 | Presión control por encima de PEEP en VM invasiva                                       |     2400142 |      17 |         8 |
| 218 | sensibilidad extremidad inferior izquierda                                              |     2100110 |      15 |         8 |
| 219 | Flujo de entrada de oxígeno al VM anestesia                                             |     2400107 |      12 |         8 |
| 220 | sensibilidad extremidad superior derecha                                                |     2100107 |      11 |         8 |
| 221 | Potencial de hidrogeniones en orina neonatología                                        |     2200061 |      10 |         8 |
| 222 | Incoordinación toracoabdominal                                                          |     2100067 |       9 |         8 |
| 223 | Uso de musculatura accesoria                                                            |     2100068 |       9 |         8 |
| 224 | Tiempo de llenado capilar                                                               |     2100096 |       9 |         8 |
| 225 | coloración extremidad superior derecha                                                  |     2100111 |       8 |         8 |
| 226 | Presión arterial no invasiva contínua sistólica                                         |     2000040 |       8 |         8 |
| 227 | Presión arterial no invasiva contínua media                                             |     2000041 |       8 |         8 |
| 228 | Presión arterial no invasiva contínua diastólica                                        |     2000042 |       8 |         8 |
| 229 | coloración extremidad superior izquierda                                                |     2100112 |       7 |         8 |
| 230 | Sistema de humidificación para lentillas de alto flujo en neonatología                  |     2400054 |       7 |         8 |
| 231 | Fracción inspiratoria de oxígeno en lentillas de alto flujo en neonatología             |     2400056 |       7 |         8 |
| 232 | Temperatura rectal                                                                      |     2000061 |       6 |         8 |
| 233 | Dispositivo para la administración de oxígeno en respiración espontánea en neonatología |     2400034 |       5 |         8 |
| 234 | Frecuencia cardíaca por auscultación                                                    |     2000021 |       4 |         8 |
| 235 | Intensidad de pulso arteria radial izquierda                                            |     2100087 |       4 |         8 |
| 236 | Fracción de Carboxihemoglobina                                                          |     2200007 |       3 |         8 |
| 237 | Fracción inspiratoria de oxígeno en respiración espontánea en neonatología              |     2400037 |       3 |         8 |
| 238 | Fracción de Metahemoglobina                                                             |     2200008 |       2 |         8 |
| 239 | Intensidad de pulso arteria humeral izquierda                                           |     2100085 |       2 |         8 |
| 240 | Temperatura cutánea                                                                     |     2000063 |       2 |         8 |
| 241 | Hematuria en tira reactiva                                                              |     2200053 |       2 |         8 |
| 242 | Capacidad vital en respiración espontánea                                               |     2100062 |       1 |         8 |
| 243 | Glucosuria en tira reactiva                                                             |     2200052 |       1 |         8 |
| 244 | Nebulización con oxígeno o con aire                                                     |     2400040 |       1 |         8 |
| 245 | Proteinuria en tira reactiva                                                            |     2200054 |       1 |         8 |