-- We want to make a query that returns patient_ref, natio_ref and desc for all following
-- natio_refs: 'KH', 'KI', 'BZ', 'WS'. Among them there are countries that are not in the demo_events table ('KH', 'KI')
-- and countries that are in the demo_events table ('BZ', 'WS'). The answer should preserve all the required countries,
-- whether they appear in the demo_events table or not.

-- To do this, this query uses a RIGHT JOIN: two tables are joined by a common field and all results from the second table
-- (right table, hence RIGHT JOIN) are preserved in the answer.

SELECT
	a.patient_ref,
	b.natio_ref,
    b.descr
FROM demo_events a  -- It is the left table.
RIGHT JOIN natio_dic b  -- It is the right table.
	ON a.natio_ref = b.natio_ref
WHERE
	b.natio_ref IN ('KH', 'KI', 'BZ', 'WS');