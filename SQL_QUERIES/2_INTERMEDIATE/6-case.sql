-- CASE reflects a conditional logic; based on the result of the urea,
-- it is labeled as high, low or normal in the 'resultado_categorico' column.

SELECT
	b.descr,
    a.result_num,
    b.units,
    CASE WHEN a.result_num > 54 THEN 'high'
		 WHEN a.result_num < 12 THEN 'low'
         WHEN a.result_num >= 12 AND a.result_num <= 54 THEN 'normal'
         END AS resultado_categorico
FROM
	lab_events a,
    lab_dic b
WHERE
	a.lab_ref = b.lab_ref AND
	a.lab_ref = 100206;