-- Returns the calculation of the average of urea for all laboratory records.

SELECT
	AVG(result_num) AS media_urea
FROM
	lab_events
WHERE
	lab_ref = 100206;