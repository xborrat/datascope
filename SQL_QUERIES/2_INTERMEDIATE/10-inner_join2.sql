-- Linking with the 2 previous examples (8-right_join.sql and 9-left_join.sql), we repeat the concept of INNER JOIN.

-- In this case, we want to make a query that returns natio_ref (between 'KH', 'KI', 'BZ' and 'WS'), desc and patient_ref.
-- To this end, we combine the natio_dic and demo_events tables. The answer should preserve only the countries
-- common to both tables. Remember that there are countries that are not in the demo_events table ('KH', 'KI') and countries that
-- are in the demo_events table ('BZ', 'WS').

-- To do this, this query uses an INNER JOIN: two tables are joined by a common field and the answer offers exclusively
-- those rows in which the common field contains the same value in the two tables.

SELECT
	a.natio_ref,
    a.descr,
    b.patient_ref
FROM natio_dic a  -- It is the left table.
INNER JOIN demo_events b  -- It is the right table.
	ON a.natio_ref = b.natio_ref
WHERE
	a.natio_ref IN ('KH', 'KI', 'BZ', 'WS');