-- With the UNION clause it is possible to show in a single table the results of the first table
-- (the one above) with the results of the second table (the one below).

-- The first and second tables must have the same number of columns and the columns
-- must contain data of the same type in both tables.

SELECT
    patient_ref,
    birth_date,
    sex,
    natio_ref
FROM
    demo_events
WHERE
    natio_ref = 'GD'

UNION

SELECT
    patient_ref,
    birth_date,
    sex,
    natio_ref
FROM
    demo_events
WHERE
    natio_ref = 'FM';