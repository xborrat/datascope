-- Returns the maximum and minimum values of urea recorded in the laboratory.

SELECT
	MIN(result_num) AS min_urea,
    MAX(result_num) AS max_urea
FROM
	lab_events
WHERE
	lab_ref = 100206;