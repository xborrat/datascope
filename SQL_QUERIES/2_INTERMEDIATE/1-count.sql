-- This query returns the count of tests performed by HAH. COUNT(lab_ref) returns
-- the number of non-null rows; COUNT(*) returns the count of all rows. In this case
-- they match.

SELECT
	COUNT(lab_ref) AS recuento_pruebas
FROM
	care_level_events a,
	lab_events b
WHERE
	a.care_level_ref = b.care_level_ref AND
	care_level_type_ref = 'HAH';