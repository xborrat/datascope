-- Although it lacks clinical sense, we add the results of urea
-- (lab_ref = 100206) from these two episodes (45813093, 321097305):

SELECT
	SUM(result_num)
FROM
	lab_events
WHERE
	episode_ref IN (45813093, 321097305) AND
    lab_ref = 100206;