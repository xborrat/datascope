-- Joins the episode_events tables (alias a) and demo_events tables (alias b) using the patient_ref field (common to both tables);
-- returns the rows that have results in both tables, that is: if the episode_events table
-- has a patient_ref that is not in the demo_events table, this query will not return that patient_ref,
-- and if the demo_events table contains a patient_ref that is not in episode_events, this query will also
-- not return this last patient_ref.

SELECT
	a.patient_ref,
    a.episode_ref,
    a.mot_start_ref,
    a.mot_end_ref,
    b.birth_date
FROM episode_events a
INNER JOIN demo_events b  -- We could write only JOIN since they are equivalent but it is preferable to use INNER JOIN for clarity.
	ON a.patient_ref = b.patient_ref;