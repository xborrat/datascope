-- It is the same example as 8-right_join.sql but reversing the order of the tables.

-- We want to make a query that returns natio_ref, desc and patient_ref for all following
-- natio_refs: 'KH', 'KI', 'BZ', 'WS'. Among them there are countries that are not in the demo_events table ('KH', 'KI')
-- and countries that are in the demo_events table ('BZ', 'WS'). The answer should preserve all the required countries,
-- whether they appear in the demo_events table or not.

-- To do this, this query uses a LEFT JOIN: two tables are joined by a common field and all results from the first table
-- (left table, hence LEFT JOIN) are preserved in the answer.

SELECT
	a.natio_ref,
    a.descr,
    b.patient_ref
FROM natio_dic a  -- It is the left table.
LEFT JOIN demo_events b  -- It is the right table.
	ON a.natio_ref = b.natio_ref
WHERE
	a.natio_ref IN ('KH', 'KI', 'BZ', 'WS');