-- This query selects unique values (excludes repetitions) with the DISTINCT clause.
-- In this way, the different nationalities of the patients treated in the hospital are obtained.

SELECT DISTINCT
	b.natio_ref
FROM
	episode_events a,
    demo_events b
WHERE
	a.patient_ref = b.patient_ref;