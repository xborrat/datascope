-- With this query we are going to get all the information available in the 'datetime'
-- type data, as in the end_date column of the episode_events table.

-- We will get date, year, month, day, hour, minute and second in separate columns.

SELECT
    end_date,
    DATE(end_date),
    YEAR(end_date),
    MONTH(end_date),
    DAY(end_date),
    HOUR(end_date),
    MINUTE(end_date),
    SECOND(end_date)
FROM
    episode_events;