-- With this query we are going to obtain patient_ref, care_level_type_ref, start_date,
-- time between each admission and minutes between each admission of patients who are admitted
-- between June 1st and 2nd, 2021 in ICU or SALA; the result will be ordered by
-- care_level_type_ref and finally by start_date.

-- To do this we will split the data based on the care level, order these
-- partitions by start_date in ascending order and use the analytic function LEAD
-- (accesses the information in the next row -1 indicates 'next row',
-- a 2 would indicate second next row-), the TIMEDIFF function (calculates the difference in
-- hours:minutes:seconds) and the TIMESTAMPDIFF function (calculates the difference in the specified unit,
-- in this case MINUTE -minutes-).

SELECT
    patient_ref,
    care_level_type_ref,
    start_date,
    TIMEDIFF(
		LEAD(start_date, 1) OVER (PARTITION BY care_level_type_ref ORDER BY start_date ASC),
        start_date
	) AS tiempo_entre_ingresos,
    TIMESTAMPDIFF(
		MINUTE,
		start_date,
		LEAD(start_date, 1) OVER (PARTITION BY care_level_type_ref ORDER BY start_date ASC)
	) AS minutos_entre_ingresos
FROM
	care_level_events
WHERE
	start_date >= '2021-06-01 00:00:00' AND
    start_date < '2021-06-03 00:00:00' AND
	care_level_type_ref IN ('ICU', 'SALA')
ORDER BY
	care_level_type_ref,
    start_date;