-- This query returns patient_ref, care_level_type_ref, start_date, end_date, and
-- num_ingreso_por_nivel from the care_level_events table of patients who are admitted
-- during the month of June 2021 sorted by care level, start_date, and end_date.

-- The num_ingreso_por_nivel column is the result of the analytic function (or 'window function')
-- ROW_NUMBER after splitting the table into care levels where each level is ordered by start_date;
-- this function assigns a number from 1 to the last row of the care level;
-- the last number of the level equals the total number of admissions that level of care had
-- during the month of June 2021.

SELECT
    patient_ref,
    care_level_type_ref,
    start_date,
    end_date,
    ROW_NUMBER() OVER (PARTITION BY care_level_type_ref
						ORDER BY start_date) AS num_ingreso_por_nivel 
FROM
	care_level_events
WHERE
	start_date >= '2021-06-01 00:00:00' AND
    start_date < '2021-07-01 00:00:00'
ORDER BY
	care_level_type_ref,
    start_date,
    end_date;