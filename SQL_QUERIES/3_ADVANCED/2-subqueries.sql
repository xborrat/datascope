-- In this query, a subquery is used to select patients with the highest recorded
-- indirect bilirubin value in the laboratory. From these patients we get: episode_ref,
-- patient_ref, reason for admission, reference and name of the laboratory parameter,
-- result of that parameter, units of that parameter, date of admission and date of discharge.

SELECT
	a.episode_ref,
    a.patient_ref,
    b.descr AS motivo_ingreso,
    c.lab_ref,
    d.descr AS parametro_lab,
    c.result_num,
    d.units,
    a.start_date,
    a.end_date
FROM episode_events a
INNER JOIN mot_dic b
	ON a.mot_start_ref = b.mot_ref
INNER JOIN lab_events c
	ON a.episode_ref = c.episode_ref
INNER JOIN lab_dic d
	ON c.lab_ref = d.lab_ref
WHERE
	c.lab_ref = '100032' AND
	c.result_num = (SELECT  -- This is the subquery (query inside another query).
						MAX(result_num)
					FROM
						lab_events 
					WHERE
						lab_ref = '100032');