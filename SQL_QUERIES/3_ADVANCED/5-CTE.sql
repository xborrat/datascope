-- A COMMON TABLE EXPRESSION (CTE) is a named subquery that can be
-- reused within the same query. It is identified with the WITH clause.

-- In the following example, two CTEs are used to create an illustrative result of
-- the JOINs seen so far: the CTEs are joined in three different types of JOINs;
-- the results of these JOINs will be seen one on top of the other thanks to the use of
-- UNION (only UNION returns different rows and UNION ALL maintains duplicate rows;
-- in this case with UNION is enough since there are no duplicate rows).

WITH
    t_izq AS (
        SELECT
            n.natio_ref AS natio_ref_izq,
            n.descr AS desc_izq
        FROM
            natio_dic n
        WHERE
            n.natio_ref IN ('AT', 'IT')
    ),
    t_dcha AS (
        SELECT
            n.natio_ref AS natio_ref_dcha,
            n.descr AS desc_dcha
        FROM
            natio_dic n
        WHERE
            n.natio_ref IN ('IT', 'ES')
    )
SELECT
    *,
    'LEFT JOIN' AS Tipo_de_JOIN
FROM t_izq i
LEFT JOIN t_dcha d  -- It will show AT and IT.
    ON i.natio_ref_izq = d.natio_ref_dcha

UNION

SELECT
    *,
    'INNER JOIN' AS Tipo_de_JOIN
FROM t_izq i
INNER JOIN t_dcha d  -- It will show IT.
    ON i.natio_ref_izq = d.natio_ref_dcha

UNION

SELECT
    *,
    'RIGHT JOIN' AS Tipo_de_JOIN
FROM t_izq i
RIGHT JOIN t_dcha d  -- It will show IT and ES.
    ON i.natio_ref_izq = d.natio_ref_dcha;