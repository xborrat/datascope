-- With these two queries, we will obtain a list of all possible combinations
-- of episode_ref, diagnosis and movement of patients in the psychiatry service
-- (PSI) who were admitted to the conventional hospitalization room (WARD) and had their
-- episodes end in the year 2022 due to two situations: escape (query 1) and
-- discharge to home (query 2); 320014 is the mot_ref for 'Escape', 320008
-- is the mot_ref for 'Home Destination'.

SELECT
	e.episode_ref,
    dd.descr,
    d.diag_ref,
    m.mov_id,
    md.descr,
    c.care_level_ref,
    c.care_level_type_ref,
    m.ou_med_ref
FROM episode_events e
INNER JOIN mot_dic md
	ON e.mot_end_ref = md.mot_ref
INNER JOIN diag_events d
    ON e.episode_ref = d.episode_ref
INNER JOIN diag_dic dd
    ON d.diag_ref = dd.diag_ref
INNER JOIN mov_events m
    ON e.episode_ref = m.episode_ref
INNER JOIN care_level_events c
    ON c.care_level_ref = m.care_level_ref
WHERE
    m.ou_med_ref = 'PSI' AND
    c.care_level_type_ref = 'WARD' AND
    e.mot_end_ref = 320014 AND
    e.end_date < '2023-01-01 00:00:00' AND
    e.end_date >= '2022-01-01 00:00:00'
ORDER BY
    e.episode_ref,
    dd.descr;


SELECT
    e.episode_ref,
    dd.descr,
    d.diag_ref,
    m.mov_id,
    md.descr,
    c.care_level_ref,
    c.care_level_type_ref,
    m.ou_med_ref
FROM episode_events e
INNER JOIN mot_dic md
	ON e.mot_end_ref = md.mot_ref
INNER JOIN diag_events d
    ON e.episode_ref = d.episode_ref
INNER JOIN diag_dic dd
    ON d.diag_ref = dd.diag_ref
INNER JOIN mov_events m
    ON e.episode_ref = m.episode_ref
INNER JOIN care_level_events c
    ON c.care_level_ref = m.care_level_ref
WHERE
    m.ou_med_ref = 'PSI' AND
    c.care_level_type_ref = 'WARD' AND
    e.mot_end_ref = 320008 AND
    e.end_date < '2023-01-01 00:00:00' AND
    e.end_date >= '2022-01-01 00:00:00'
ORDER BY
    e.episode_ref,
    dd.descr;


-- The following query will count the number of episodes for each of these two episode
-- end reasons in PSI, SALA and ending in the year 2022 using the previous queries but
-- within a 'common table expression':

WITH
	t_fuga AS (
		SELECT DISTINCT
			e.episode_ref
		FROM episode_events e
		INNER JOIN mot_dic md
			ON e.mot_end_ref = md.mot_ref
		INNER JOIN diag_events d
			ON e.episode_ref = d.episode_ref
		INNER JOIN diag_dic dd
			ON d.diag_ref = dd.diag_ref
		INNER JOIN mov_events m
			ON e.episode_ref = m.episode_ref
		INNER JOIN care_level_events c
			ON c.care_level_ref = m.care_level_ref
		WHERE
			m.ou_med_ref = 'PSI' AND
			c.care_level_type_ref = 'WARD' AND
			e.mot_end_ref = 320014 AND
			e.end_date < '2023-01-01 00:00:00' AND
			e.end_date >= '2022-01-01 00:00:00'
    ),
    t_alta_dom AS (
		SELECT DISTINCT
			e.episode_ref
		FROM episode_events e
		INNER JOIN mot_dic md
			ON e.mot_end_ref = md.mot_ref
		INNER JOIN diag_events d
			ON e.episode_ref = d.episode_ref
		INNER JOIN diag_dic dd
			ON d.diag_ref = dd.diag_ref
		INNER JOIN mov_events m
			ON e.episode_ref = m.episode_ref
		INNER JOIN care_level_events c
			ON c.care_level_ref = m.care_level_ref
		WHERE
			m.ou_med_ref = 'PSI' AND
			c.care_level_type_ref = 'WARD' AND
			e.mot_end_ref = 320008 AND
			e.end_date < '2023-01-01 00:00:00' AND
			e.end_date >= '2022-01-01 00:00:00'
    )
SELECT
	'episodios de fuga' AS tipo_episodio,
    COUNT(*) AS recuento_episodios
FROM
	t_fuga

UNION

SELECT
	'episodios de alta a domicilio' AS tipo_episodio,
    COUNT(*) AS recuento_episodios
FROM
	t_alta_dom;