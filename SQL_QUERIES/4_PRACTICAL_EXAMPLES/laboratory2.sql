-- Query to find patients who have been in the ICU and had lab values above
-- a certain threshold using the datascope reference and between two dates.
-- Example: SODIUM (datascope reference 100190) bigger than 150 and dates between
-- 2021-01-01 and 2022-01-01.
-- Part of the query to modify: 100190, 2021-01-01, 2022-01-01, ICU, 150.

select datascope.lab_events.patient_ref, datascope.lab_events.episode_ref, datascope.lab_events.extrac_date, datascope.lab_events.result_date, datascope.lab_events.ou_med_ref, datascope.lab_events.ou_loc_ref, 
datascope.lab_events.care_level_ref, lab_ref, datascope.lab_events.result_num, datascope.lab_events.result_txt from datascope.mov_events, datascope.lab_events, datascope.care_level_events  where 
datascope.mov_events.care_level_ref= datascope.lab_events.care_level_ref and datascope.mov_events.care_level_ref= datascope.care_level_events.care_level_ref 
and care_level_type_ref = 'ICU' and result_num >150 and extrac_date >= '2021-01-01' and extrac_date <= '2022-01-01' and lab_ref = 100190;