-- The first query searches for a diagnosis based on a reference from the CIE-10,
-- in this example it is searching for C18. The part of the query to be modified
-- is 'C18%'
SELECT * FROM datascope.diag_sap_dic where cie_ref LIKE 'C18%';

-- The second query is used to find the SAP reference, given the DataNex reference.
-- In this example, it's searching for 'Tumor maligno del colon, parte no especificada'
-- which has a DataNex reference of 6012772. The part of the query to be modified
-- is 6012772.
SELECT * FROM datascope.diag_sap_dic where diag_ref = 6012772;

-- The third query is used to find the patients, given the DataNex reference. In this
-- example, it's searching for 'Tumor maligno del colon, parte no especificada' which has
-- a DataNex reference of 6012772. The part of the query to be modified is 6012772.
SELECT * FROM datascope.diag_events where diag_ref = 6012772;

-- The fourth query is used to find the patients, given the SAP reference. In this example,
-- it's searching for C18% which corresponds to SAP references of C18%. The part of the query
-- to be modified is C18%.
SELECT * FROM datascope.diag_events where diag_ref IN(SELECT distinct diag_ref FROM datascope.diag_sap_dic where cie_ref LIKE 'C18%');

-- The fifth query searches for patients who have been diagnosed with 'Tumor maligno del colon,
-- parte no especificada' (DataNex reference 6012772) between the dates of 2021-01-01 and
-- 2022-01-01. The parts of the query to be modified are 6012772, 2021-01-01 and 2022-01-01
-- respectively.
SELECT * FROM datascope.diag_events where diag_ref = 6012772 and reg_date >= '2021-01-01' and reg_date <= '2022-01-01';