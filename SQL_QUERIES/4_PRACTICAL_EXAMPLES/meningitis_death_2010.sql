-- Selects patients with primary diagnosis of meningitis who have died in or after the year 2010.

SELECT
	de.patient_ref,
    dd.descr,
    de.class,
    exe.exitus_date
FROM diag_dic dd
INNER JOIN diag_events de
	ON dd.diag_ref = de.diag_ref
INNER JOIN exitus_events exe
	ON de.patient_ref = exe.patient_ref
WHERE
	dd.diag_ref = '6000476' AND
    de.class = 'P' AND
    YEAR(exe.exitus_date) >= 2010;