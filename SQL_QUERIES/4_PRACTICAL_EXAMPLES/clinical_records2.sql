-- Retrieve patients who have been in the ICU based on a datascope reference,
-- between two dates and only select clinical records above a certain threshold.
-- Example: AXILLARY TEMPERATURE (datascope reference 2000052) bigger than 37.5
-- between dates 2021-01-01 and 2022-01-01.
-- Part of the query to be modified: 2000052, 2021-01-01, 2022-01-01, ICU, 37.5.

select datascope.rc_events.patient_ref, datascope.rc_events.episode_ref, datascope.rc_events.rc_ref, datascope.rc_events.ou_med_ref, datascope.rc_events.ou_loc_ref, 
datascope.rc_events.care_level_ref, datascope.rc_events.result_date, datascope.rc_events.result_num, datascope.rc_events.result_txt, datascope.rc_events.meas_type from 
datascope.mov_events, datascope.rc_events, datascope.care_level_events  where 
datascope.mov_events.care_level_ref= datascope.rc_events.care_level_ref and datascope.mov_events.care_level_ref= datascope.care_level_events.care_level_ref 
and care_level_type_ref = 'ICU' and result_num >37.5 and result_date >= '2021-01-01' and result_date <= '2022-01-01' and rc_ref = 2000052