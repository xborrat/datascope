-- Search for a clinical record based on a name. The example is done with TEMP.
-- Part to modify in the query: '%TEMP%'.
SELECT * FROM datascope.rc_dic where datascope.rc_dic.descr LIKE '%TEMP%';


-- Search for a DataScope reference from a SAP reference. The example is done
-- with AXILLARY TEMPERATURE which corresponds to the SAP references: TEMP_AX, TEMP_AXI.
-- Part to modify in the query: IN ('TEMP_AX', 'TEMP_AXI').
SELECT * FROM datascope.rc_sap_dic where rc_sap_ref IN('TEMP_AX','TEMP_AXI');


-- If you know the DataScope reference, find the SAP reference. The example is done
-- with AXILLARY TEMPERATURE which is the DataScope reference 2000052.
-- Part to modify in the query: 2000052.
SELECT * FROM datascope.rc_sap_dic where rc_ref = 2000052;


-- Search for patients knowing the DataScope reference. The example is done with
-- AXILLARY TEMPERATURE which is the DataScope reference 2000052.
-- Part to modify in the query: 2000052.
SELECT * FROM datascope.rc_events where rc_ref = 2000052;


-- Find patients based on a name of a clinical record. The example is done with TEMP.
-- Part to modify in the query: '%TEMP%'.
SELECT * FROM datascope.rc_events, datascope.rc_dic where datascope.rc_events.rc_ref= datascope.rc_dic.rc_ref and datascope.rc_dic.descr LIKE '%TEMP%';


-- Find patients knowing the SAP reference. The example is done with AXILLARY TEMPERATURE
-- which corresponds to the SAP references: TEMP_AX, TEMP_AXI.
-- Part to modify in the query: ('TEMP_AX', 'TEMP_AXI').
SELECT * FROM datascope.rc_events where rc_ref IN(SELECT distinct rc_ref FROM datascope.rc_sap_dic where rc_sap_ref IN('TEMP_AX','TEMP_AXI'));


-- Search for patient records using a reference from datascope within a date range.
-- Example: axillary temperature (datascope reference 2000052) between 2021-01-01
-- and 2022-01-01.
-- Part to modify in the query: 2000052, 2021-01-01, 2022-01-01.
SELECT * FROM datascope.rc_events where rc_ref = 2000052 and result_date >= '2021-01-01' and result_date <= '2022-01-01';


-- Search for patient records using a reference from datascope only selecting
-- values above a certain threshold. Example: axillary temperature (datascope
-- reference 2000052) with values greater than 37.5.
-- Part to modify in the query: 2000052 and 37.5.
SELECT * FROM datascope.rc_events where rc_ref = 2000052 and result_num >37.5;


-- Search for patient records using a reference from datascope and only selecting
-- those ordered by a certain service. Example: axillary temperature (datascope
-- reference 2000052) ordered by the Cardiology service (CAR).
-- Part to modify in the query: 2000052 and CAR.
SELECT * FROM datascope.rc_events where rc_ref = 2000052 and ou_med_ref = 'CAR';