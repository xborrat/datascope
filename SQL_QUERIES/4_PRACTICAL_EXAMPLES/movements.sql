-- Search for patients based on a location. Example is performed with E073.
-- Part of query to modify: E073.
SELECT * FROM datascope.mov_events where ou_loc_ref = 'E073';


-- Search for patients based on a service. Example is performed with CAR.
-- Part of query to modify: CAR.
SELECT * FROM datascope.mov_events where ou_med_ref = 'CAR';


-- Search for movements performed by the hospital that correspond to a
-- care level. Example is performed with ICU.
-- Part of query to modify: ICU.
select * from datascope.mov_events, datascope.care_level_events  where datascope.mov_events.care_level_ref= datascope.care_level_events.care_level_ref 
and care_level_type_ref = 'ICU' and mov_events.start_date>'2020-01-01';