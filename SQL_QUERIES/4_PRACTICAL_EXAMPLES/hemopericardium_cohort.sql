-- Selects a cohort of patients with hemopericardium as a complication
-- subsequent to acute myocardial infarction whose age is 50 years or less.

WITH t1 AS (
	SELECT DISTINCT
		de.patient_ref,
		dd.descr,
		TIMESTAMPDIFF(YEAR, dme.birth_date, NOW()) AS years
	FROM diag_events de
	INNER JOIN diag_dic dd
		ON de.diag_ref = dd.diag_ref
	INNER JOIN demo_events dme
		ON de.patient_ref = dme.patient_ref
	WHERE
		de.diag_ref = 6004408
)
SELECT
	*
FROM	
	t1
WHERE
	years <= 50;