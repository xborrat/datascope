-- These two queries show the amount of lab tests and clinical records, respectively.

SELECT
	ld.descr,
    COUNT(*) AS lab_test_count
FROM lab_events le
INNER JOIN lab_dic ld
	ON le.lab_ref = ld.lab_ref
GROUP BY
	ld.descr
ORDER BY
	lab_test_count DESC;


SELECT
	rd.descr,
    COUNT(*) AS clinical_record_count
FROM rc_events re
INNER JOIN rc_dic rd
	ON re.rc_ref = rd.rc_ref
GROUP BY
	rd.descr
ORDER BY
	clinical_record_count DESC;