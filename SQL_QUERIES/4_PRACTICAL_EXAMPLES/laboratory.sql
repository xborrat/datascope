-- Search for a laboratory test by name. The example is done with SODIUM.
-- Part to modify in the query: '%SODI%'.
SELECT * FROM datascope.lab_dic where datascope.lab_dic.descr LIKE '%SODI%';


-- Search based on a DataScope reference from a SAP reference. The example is done
-- with SODIUM that corresponds to the SAP references: LAB2507, LABNAST, LABNASV,
-- LABRPANA, LABRPVNA.
-- Part to modify in the query: IN('LAB2507','LABNAST','LABNASV', 'LABRPANA','LABRPVNA').
SELECT * FROM datascope.lab_sap_dic where lab_sap_ref IN('LAB2507','LABNAST','LABNASV', 'LABRPANA','LABRPVNA');


-- If you know the DataScope reference, find the SAP reference. The example is
-- done with SODIUM that is the DataScope reference 100190.
-- Part to modify in the query: 100190.
SELECT * FROM datascope.lab_sap_dic where lab_ref = 100190;


-- Query patients by knowing the DataScope reference. The example is done with
-- SODIUM that is the DataScope reference 100190.
-- Part to modify in the query: 100190.
SELECT * FROM datascope.lab_events where lab_ref = 100190;


-- Query patients by a laboratory test name. The example is done with SODIUM.
-- Part to modify in the query: '%SODI%'.
SELECT * FROM datascope.lab_events, datascope.lab_dic where datascope.lab_events.lab_ref= datascope.lab_dic.lab_ref and datascope.lab_dic.descr LIKE '%SODI%';


-- Query patients by knowing the SAP reference. The example is done with
-- SODIUM that corresponds to the SAP references: LAB2507, LABNAST, LABNASV,
-- LABRPANA, LABRPVNA.
-- Part to modify in the query: ('LAB2507','LABNAST','LABNASV', 'LABRPANA','LABRPVNA').
SELECT * FROM datascope.lab_events where lab_ref IN(SELECT distinct lab_ref FROM datascope.lab_sap_dic where lab_sap_ref IN('LAB2507','LABNAST','LABNASV', 'LABRPANA','LABRPVNA'));


-- Query patients by knowing the DataScope reference and between two dates.
-- The example is done with SODIUM that is the DataScope reference 100190
-- and a date greater than 2021-01-01 until 2022-01-01.
-- Part to modify in the query: 100190, 2021-01-01, 2022-01-01.
SELECT * FROM datascope.lab_events where lab_ref = 100190 and extrac_date >= '2021-01-01' and extrac_date <= '2022-01-01';


-- Search for patients with a laboratory test reference only selecting
-- values greater than X. The example is for SODIUM, which has a datascope
-- reference of 100190 and we look for a sodium value greater than 150.
-- Part of the query to modify: 100190 and 150.
SELECT * FROM datascope.lab_events where lab_ref = 100190 and result_num >150;


-- Search for patients with a datascope reference requested by a specific service.
-- The example is for SODIUM, which has a datascope reference of 100190 and is
-- requested by the Cardiology service (CAR).
-- Part of the query to modify: 100190 and CAR.
SELECT * FROM datascope.lab_events where lab_ref = 100190 and ou_med_ref = 'CAR';