-- Select the patient with patient_ref 41317569:

SELECT
	*
FROM episode_events ee
INNER JOIN diag_events de
	ON ee.episode_ref = de.episode_ref
WHERE
	ee.patient_ref = 41317569
ORDER BY
	ee.start_date;


-- From the intermediate date 2019-05-19 we search for current
-- illness, medical history and future illnesses:

SELECT
	'ACTUAL' AS patología,
	de.patient_ref,
    de.episode_ref,
	de.diag_ref,
    dd.descr
FROM diag_events de
INNER JOIN diag_dic dd
	ON de.diag_ref = dd.diag_ref
WHERE
	de.patient_ref = 41317569 AND
    de.reg_date = '2019-05-19'

UNION

SELECT
	'PASADA' AS patología,
	de.patient_ref,
    de.episode_ref,
	de.diag_ref,
    dd.descr
FROM diag_events de
INNER JOIN diag_dic dd
	ON de.diag_ref = dd.diag_ref
WHERE
	de.patient_ref = 41317569 AND
    de.reg_date < '2019-05-19'

UNION

SELECT
	'FUTURA' AS patología,
	de.patient_ref,
    de.episode_ref,
	de.diag_ref,
    dd.descr
FROM diag_events de
INNER JOIN diag_dic dd
	ON de.diag_ref = dd.diag_ref
WHERE
	de.patient_ref = 41317569 AND
    de.reg_date > '2019-05-19';



-- Improvements:

WITH
	t_actual AS (
		SELECT
			'ACTUAL' AS patología,
			de.patient_ref AS pax,
            GROUP_CONCAT(de.diag_ref ORDER BY de.diag_ref SEPARATOR ' | ') AS diagnósticos_actuales_ref,
			GROUP_CONCAT(dd.descr ORDER BY de.diag_ref SEPARATOR ' | ') AS diagnósticos_actuales
		FROM diag_events de
		INNER JOIN diag_dic dd
			ON de.diag_ref = dd.diag_ref
		WHERE
			de.patient_ref = 41317569 AND
			de.reg_date = '2019-05-19'
		GROUP BY
			de.patient_ref),
	t_pasada AS (
		SELECT
			'PASADA' AS patología,
			de.patient_ref AS pax,
			GROUP_CONCAT(de.diag_ref ORDER BY de.diag_ref SEPARATOR ' | ') AS antecedentes_ref,
			GROUP_CONCAT(dd.descr ORDER BY de.diag_ref SEPARATOR ' | ') AS antecedentes
		FROM diag_events de
		INNER JOIN diag_dic dd
			ON de.diag_ref = dd.diag_ref
		WHERE
			de.patient_ref = 41317569 AND
			de.reg_date < '2019-05-19'
		GROUP BY
			de.patient_ref),
	t_futura AS (
		SELECT
			'FUTURA' AS patología,
			de.patient_ref AS pax,
			GROUP_CONCAT(de.diag_ref ORDER BY de.diag_ref SEPARATOR ' | ') AS diagnósticos_futuros_ref,
			GROUP_CONCAT(dd.descr ORDER BY de.diag_ref SEPARATOR ' | ') AS diagnósticos_futuros
		FROM diag_events de
		INNER JOIN diag_dic dd
			ON de.diag_ref = dd.diag_ref
		WHERE
			de.patient_ref = 41317569 AND
			de.reg_date > '2019-05-19'
		GROUP BY
			de.patient_ref)
SELECT
	a.pax,
    a.diagnósticos_actuales_ref,
    a.diagnósticos_actuales,
    p.antecedentes_ref,
    p.antecedentes,
    f.diagnósticos_futuros_ref,
    f.diagnósticos_futuros
FROM t_actual a
INNER JOIN t_pasada p
	ON a.pax = p.pax
INNER JOIN t_futura f
	ON p.pax = f.pax;



-- CREATE PROCEDURE: it works with TEST, not with DATASCOPE.

DELIMITER $$
CREATE PROCEDURE iter_dates()
BEGIN
    DECLARE i INT DEFAULT 0;
    WHILE i <= 2 DO
        IF i = 0 THEN
            SELECT
				'ACTUAL' AS patología,
				de.patient_ref,
				GROUP_CONCAT(de.diag_ref ORDER BY de.diag_ref SEPARATOR ' | '),
				GROUP_CONCAT(dd.descr ORDER BY de.diag_ref SEPARATOR ' | ')
            FROM diag_events de
			INNER JOIN diag_dic dd
				ON de.diag_ref = dd.diag_ref
            WHERE
				de.patient_ref = 41317569 AND
                de.reg_date = '2019-05-19';
        ELSEIF i = 1 THEN
            SELECT
				'PASADA' AS patología,
				de.patient_ref,
				GROUP_CONCAT(de.diag_ref ORDER BY de.diag_ref SEPARATOR ' | '),
				GROUP_CONCAT(dd.descr ORDER BY de.diag_ref SEPARATOR ' | ')
            FROM diag_events de
			INNER JOIN diag_dic dd
				ON de.diag_ref = dd.diag_ref
            WHERE
				de.patient_ref = 41317569 AND
                de.reg_date < '2019-05-19';
        ELSEIF i = 2 THEN
            SELECT
				'FUTURA' AS patología,
				de.patient_ref,
				GROUP_CONCAT(de.diag_ref ORDER BY de.diag_ref SEPARATOR ' | '),
				GROUP_CONCAT(dd.descr ORDER BY de.diag_ref SEPARATOR ' | ')
            FROM diag_events de
			INNER JOIN diag_dic dd
				ON de.diag_ref = dd.diag_ref
            WHERE
				de.patient_ref = 41317569 AND
                de.reg_date < '2019-05-19';
        END IF;
        SET i = i + 1;
    END WHILE;
END$$
DELIMITER;