-- Retrieves the diag_ref for diagnosis that contain the word 'peritonitis'.

SELECT
	*
FROM
	diag_dic dd
WHERE
    dd.descr LIKE '%peritonitis%';


-- We select the diag_ref that are of interest:
-- 6000553, 6000554, 6001810, 6008026, 6011920, 6011921, 6011922, 6012101.
-- We count the number of episodes with those peritonitis:

SELECT
	dd.descr,
    COUNT(*) AS recuento_peritonitis
FROM diag_events de
INNER JOIN diag_dic dd
	ON de.diag_ref = dd.diag_ref
WHERE
    de.diag_ref IN (6000553, 6000554, 6001810, 6008026, 6011920, 6011921, 6011922, 6012101)
GROUP BY
	de.diag_ref;