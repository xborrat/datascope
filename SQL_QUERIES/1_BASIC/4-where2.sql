-- This SQL code selects the columns "patient_ref" and "care_level_type_ref"
-- from the table "care_level_events" and only returns the rows where the
-- "care_level_type_ref" is HAH.

SELECT
	patient_ref,
	care_level_type_ref
FROM
	care_level_events
WHERE
	care_level_type_ref = 'HAH';