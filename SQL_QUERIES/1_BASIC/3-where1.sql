-- This SQL code selects the columns "patient_ref" and "care_level_type_ref"
-- from the table "care_level_events" where the column "care_level_type_ref"
-- has the value "HAH" or "WARD". This will return all the events of home
-- hospitalization level and conventional hospitalization level ("HDOM" or "SALA").

SELECT
	patient_ref,
	care_level_type_ref
FROM
	care_level_events
WHERE
	care_level_type_ref IN ('HAH', 'WARD');