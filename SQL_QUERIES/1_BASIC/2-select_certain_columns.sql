-- This SQL code selects the columns "patient_ref", "episode_ref" and "diag_ref"
-- from the table "diag_events". This means that all the values of these columns
-- will be retrieved for each row in the "diag_events" table.


SELECT
	patient_ref,
	episode_ref,
	diag_ref
FROM
	diag_events;