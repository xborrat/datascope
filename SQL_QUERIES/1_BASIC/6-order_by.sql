-- This SQL code selects the columns "birth_date" and "patient_ref"
-- from the table "demo_events" and returns the rows where the "patient_ref"
-- is 16900863 or 56875093 or if the birth date is 2020-04-03. It orders the
-- result by birth date in descending order and finally orders by patient_ref
-- in ascending order (by default).

SELECT
	birth_date,
    patient_ref
FROM
    demo_events
WHERE
	patient_ref IN ('16900863', '56875093') OR
    DATE(birth_date) = '2020-04-03'
ORDER BY
	DATE(birth_date) DESC,
    patient_ref;