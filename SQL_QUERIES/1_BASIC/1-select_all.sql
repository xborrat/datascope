-- This is an SQL query that requests to select all fields (*)
-- from the table "diag_events". This means that a set of rows with
-- all the data stored in the table "diag_events" will be returned.

SELECT
	*
FROM
	diag_events;