-- This SQL code selects the columns "patient_ref", "episode_ref",
-- "start_date" and "mot_start_ref" from the table "episode_events"
-- and only returns the rows where the "start_date" is '2021-10-14'.
-- The "DATE(start_date)" function is used to obtain only the date
-- without the time:minute:second of the "start_date" column.

SELECT
	patient_ref,
    episode_ref,
    start_date,
    mot_start_ref
FROM
	episode_events
WHERE
	DATE(start_date) = '2021-10-14';