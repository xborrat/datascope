# Repository for DataNex database users

This repository is used to store important information about the DataNex database.

It is conceived to be explored through its wiki page, whose link is provided here: https://gitlab.clinic.cat/datalake/datascope/-/wikis/home.

&nbsp;

## Main files

It includes two main files:

  - MANUAL.pdf: it describes the database.

  - MAP.png: it is a database map.

&nbsp;

## SQL_QUERIES folder:

It contains several subfolders:

  - 1_BASIC: it contains basic level SQL queries.

  - 2_INTERMEDIATE: it contains intermediate level SQL queries.

  - 3_ADVANCED: it contains advanced level SQL queries.

  - 4_PRACTICAL_EXAMPLES: it contains practical examples of SQL queries.

&nbsp;

To view and execute the SQL scripts, you must open them in MySQL Workbench, select Datascope and then execute them:

  - Open MySQL Workbench.

  - Select Datascope (double click on the database connection with the left button of the mouse) in the menu SCHEMAS (left margin).

  - File > Open SQL Script > select the desired file.
  
  - Execute (the lightning icon located at the top of the application).

&nbsp;

## Documentation folder:

The repository also includes a Documentation folder, which contains two subfolders:

  - original: original documents in .docx format; it is the source of the information about DataNex.

  - processed: processed documents in .docx format from the 'original' folder; MANUAL.docx and table_graph.docx generate MANUAL.pdf and MAP.png, respectively (these two last files are at the repository root).
